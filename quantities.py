#!/usr/bin/env pythoh3

'''
Maneja un diccionario con cantidades
'''

import sys

items = {}

def op_add():
    """Add an item and quantity from argv to the items dictionary"""
    item = sys.argv[0]
    quantity = int(sys.argv[1])
    adlist = {item:quantity}
    items.update(adlist)
    return items


def op_items():
    """Print all items, separated by spaces"""
    for k in items.keys():
        print(k, end=" ")

def op_all():
    """Print all items and quantities, separated by spaces"""
    for i in items.items():
        print(i)

def op_sum():
    """Print sum of all quantities"""
    sulist = sum(items.values())
    print(sulist)

def main():
    while sys.argv != 0:
        op = sys.argv.pop(0)
        if op == "add":
            op_add()
        elif op == "items":
            op_items()
        elif op == "sum":
            op_sum()
        elif op == "all":
            op_all()


if __name__ == '__main__':
    main()

